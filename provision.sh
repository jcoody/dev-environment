#!/usr/bin/env bash


# Determine if this machine has already been provisioned
# Basically, run everything after this command once, and only once
if [ -f "/var/vagrant_provision" ]; then 
    exit 0
fi

function say {
    printf "\n--------------------------------------------------------\n"
    printf "\t$1"
    printf "\n--------------------------------------------------------\n"
}

db='databasename'

# Install Apache
say "Installing Apache and setting it up."
    # Update aptitude library
    apt-get update >/dev/null 2>&1
    # Install apache2 
    apt-get install -y apache2 >/dev/null 2>&1
    # Remove /var/www path
    rm -rf /var/www
    # Symbolic link to /vagrant/site path
    ln -fs /vagrant /var/www
    # Enable mod_rewrite
    a2enmod rewrite

# Install mysql
say "Installing MySQL."
    apt-get update
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'
    apt-get install -y mysql-server
    sed -i -e 's/127.0.0.1/0.0.0.0/' /etc/mysql/my.cnf
    restart mysql
    mysql -u root -p vagrant mysql <<< "GRANT ALL ON *.* TO 'root'@'%'; FLUSH PRIVILEGES;"


say "Installing handy packages"
    apt-get install -y curl git-core ftp unzip vim colordiff

say "Installing PHP Modules"
    # Install php5, libapache2-mod-php5, php5-mysql curl php5-curl
    apt-get install -y php5 php5-cli php5-common php5-dev php5-imap libapache2-mod-php5 php5-mysql php5-curl

say "Installing phpMyAdmin"
    echo 'phpmyadmin phpmyadmin/dbconfig-install boolean true' | debconf-set-selections
    echo 'phpmyadmin phpmyadmin/app-password-confirm password vagrant' | debconf-set-selections
    echo 'phpmyadmin phpmyadmin/mysql/admin-pass password vagrant' | debconf-set-selections
    echo 'phpmyadmin phpmyadmin/mysql/app-pass password vagrant' | debconf-set-selections
    echo 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2' | debconf-set-selections
    sudo apt-get -q -y install phpmyadmin

# Restart Apache
say "Restarting Apache"
    service apache2 restart
    cd /vagrant
    touch info.php
    echo "<?php" >> info.php
    echo "  phpinfo();" >> info.php
    echo "?>" >> info.php

# Installing Ruby
say "Installing rvm - preparing for ruby"
    curl -L https://get.rvm.io | bash -s stable
    source /etc/profile.d/rvm.sh
    rvm requirements

say "Installing ruby now... wish me luck"
    rvm install ruby
    rvm use ruby --default
    rvm rubygems current

say "Installing Bundler"
    gem install bundler
    cd /vagrant
    bundle init

say "Installing Guard and dependencies with Bundler"
    echo "group :development do" >> Gemfile
    echo "  gem 'guard'" >> Gemfile
    echo "  gem 'guard-sass', :require => false" >> Gemfile
    echo "  gem 'guard-livereload', :require => false" >> Gemfile
    echo "end" >> Gemfile
    bundle
    #Setting up the LiveReload part of the Guardfiles
    cd /usr/local/rvm/gems/ruby-*/gems/guard-livereload-*/lib/guard/livereload/templates
    sed -i 's/public\///g' Guardfile
    sed -i 's/css|js|html/css|js|html|php/g' Guardfile
    #Setting up the SASS part of the Guardfiles
    cd /usr/local/rvm/gems/ruby-*/gems/guard-sass-*/lib/guard/sass/templates
    sed -i "s/guard 'sass', :input => 'sass', :output => 'css'/guard 'sass', :input => 'sass', :output => 'css', :style => :compressed, :cache_location => '\/tmp\/sass-cache'/g" Guardfile
    cd /vagrant
    guard init

say "Installing Node"
    sudo apt-get update
    sudo apt-get install -y python-software-properties python g++ make
    sudo add-apt-repository -y ppa:chris-lea/node.js
    sudo apt-get update
    sudo apt-get install -y nodejs

say "Finishing up"
    cd /home/vagrant/
    echo "alias vguard='guard -p -l 1'" >> .bashrc
    echo "cd /vagrant" >> .bashrc
    echo "printf \"\n--------------------------------------------------------\n\"" >> .bashrc
	echo "printf \"\tThis is your ip address: \"" >> .bashrc
	echo "ifconfig eth2 | grep 'inet addr:' | cut -d: -f2 | awk '{ printf \$1 }'" >> .bashrc
	echo "printf \"\n--------------------------------------------------------\n\"" >> .bashrc
    source .bashrc

# Let this script know not to run again
touch /var/vagrant_provision